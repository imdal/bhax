#include <iostream>
#include <cmath>
#include <fstream>

class LZWBinFa
{
public:
    
    LZWBinFa ()
    {
        gyoker = new Csomopont ('/');
        fa = gyoker;
    }
    LZWBinFa (const LZWBinFa & forras):LZWBinFa()
    {
        std::cout<<"Masolso konstruktor"<<std::endl;
        if (gyoker != nullptr)
        {
            szabadit(gyoker);
            std::cout<<"Masolso konstruktor"<<std::endl;
            gyoker = copy(forras.gyoker, forras.fa);
         
        }
    }
    LZWBinFa& operator=(const LZWBinFa& forras) 
    {     
        if (forras.gyoker == nullptr)
            return *this;
            
        szabadit(gyoker);
        gyoker = copy(forras.gyoker,forras.fa);
        return *this;
    }
    LZWBinFa (LZWBinFa&& forras)
    {
        std::cout<<"Move ctor\n";
        gyoker = nullptr;
        *this = std::move(forras);
        
    }
    LZWBinFa& operator= (LZWBinFa&& forras)
    {
        std::cout<<"Move assignment ctor\n";
        std::swap(gyoker, forras.gyoker);
        return *this;
    }
    ~LZWBinFa ()
    {
        szabadit (gyoker);
    }

    
    void operator<< (char b)
    {
        if (b == '0')
        {

            if (!fa->nullasGyermek ())
            {

                Csomopont *uj = new Csomopont ('0');
 
                fa->ujNullasGyermek (uj);

                fa = gyoker;
            }
            else
            {

                fa = fa->nullasGyermek ();
            }
        }
        else
        {
            if (!fa->egyesGyermek ())
            {
                Csomopont *uj = new Csomopont ('1');
                fa->ujEgyesGyermek (uj);
                fa = gyoker;
            }
            else
            {
                fa = fa->egyesGyermek ();
            }
        }
    }

    void kiir (void)
    {

        melyseg = 0;
        kiir (gyoker, std::cout);
    }
    int getMelyseg (void);
	int szamol (void);


    friend std::ostream & operator<< (std::ostream & os, LZWBinFa & bf)
    {
        bf.kiir (os);
        return os;
    }
    void kiir (std::ostream & os)
    {
        melyseg = 0;
        kiir (gyoker, os);
    }

private:
    class Csomopont
    {
    public:
        Csomopont (char b = '/'):betu (b), balNulla (0), jobbEgy (0)
        {
        };
        ~Csomopont ()
        {
        };
        Csomopont *nullasGyermek () const
        {
            return balNulla;
        }
        Csomopont *egyesGyermek () const
        {
            return jobbEgy;
        }
        void ujNullasGyermek (Csomopont * gy)
        {
            balNulla = gy;
        }

        void ujEgyesGyermek (Csomopont * gy)
        {
            jobbEgy = gy;
        }

        char getBetu () const
        {
            return betu;
        }

    private:
        char betu;
        Csomopont *balNulla;
        Csomopont *jobbEgy;
        Csomopont (const Csomopont &);
        Csomopont & operator= (const Csomopont &);
    };

    Csomopont *fa;
    int melyseg;
    void kiir (Csomopont * elem, std::ostream & os)
    {
        if (elem != NULL)
        {
            ++melyseg;
            kiir (elem->egyesGyermek (), os);
            for (int i = 0; i < melyseg; ++i)
                os << "---";
            os << elem->getBetu () << "(" << melyseg - 1 << ")" << std::endl;
            kiir (elem->nullasGyermek (), os);
            --melyseg;
        }
    }
    Csomopont* copy (const Csomopont* forras, const Csomopont *regifa )
    {
        Csomopont* masolt = nullptr;
        if (forras != nullptr)
        {
            masolt = new Csomopont(forras->getBetu());
            
            masolt->ujEgyesGyermek(copy(forras->egyesGyermek(), regifa));
            
            masolt->ujNullasGyermek(copy(forras->nullasGyermek(),regifa));
            
            if (regifa == forras)
                fa = masolt;
        }
        return masolt;
    }
    void szabadit (Csomopont * elem)
    {
        if (elem != NULL)
        {
            szabadit (elem->egyesGyermek ());
            szabadit (elem->nullasGyermek ());
            delete elem;
        }
    }

protected:
    Csomopont *gyoker;
    int maxMelyseg,a;

    void rmelyseg (Csomopont * elem);
    void szamol (Csomopont * elem);

};

int LZWBinFa::szamol (void)
{
a = 0;
szamol(gyoker);
return a;
}



int
LZWBinFa::getMelyseg (void)
{
    melyseg = maxMelyseg = 0;
    rmelyseg (gyoker);
    return maxMelyseg - 1;
}

void
LZWBinFa::rmelyseg (Csomopont * elem)
{
    if (elem != NULL)
    {
        ++melyseg;
        if (melyseg > maxMelyseg)
            maxMelyseg = melyseg;
        rmelyseg (elem->egyesGyermek ());
        rmelyseg (elem->nullasGyermek ());
        --melyseg;
    }
}


void
LZWBinFa::szamol (Csomopont * elem)
{
    if (elem != NULL)
    {
	a++;
	szamol (elem->egyesGyermek ());
        szamol (elem->nullasGyermek ());
	
    }
}

int
main (int argc, char *argv[])
{
LZWBinFa binfa1;
LZWBinFa binfa0;
        for (int i = 0; i < 10; ++i)
        {
	binfa1 << '1';
        }
   	for (int i = 0; i < 36; ++i)
        {
	binfa0 << '0';
	}

std::cout << binfa1 << std::endl;
std::cout << binfa0;
std::cout << "\nA 0 darabszáma: " << binfa1.szamol();
std::cout << "\nAz 1 darabszáma: " << binfa0.szamol() << std::endl;

    return 0;
}
